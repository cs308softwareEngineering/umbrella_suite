/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mbenlioglu
 */
@Entity
@Table(name = "product_types")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductTypes.findAll", query = "SELECT p FROM ProductTypes p"),
    @NamedQuery(name = "ProductTypes.findByPtname", query = "SELECT p FROM ProductTypes p WHERE p.ptname = :ptname"),
    @NamedQuery(name = "ProductTypes.findByTypeDiscount", query = "SELECT p FROM ProductTypes p WHERE p.typeDiscount = :typeDiscount")})
public class ProductTypes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ptname")
    private String ptname;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "typeDiscount")
    private Double typeDiscount;

    public ProductTypes() {
    }

    public ProductTypes(String ptname) {
        this.ptname = ptname;
    }

    public String getPtname() {
        return ptname;
    }

    public void setPtname(String ptname) {
        this.ptname = ptname;
    }

    public Double getTypeDiscount() {
        return typeDiscount;
    }

    public void setTypeDiscount(Double typeDiscount) {
        this.typeDiscount = typeDiscount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptname != null ? ptname.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductTypes)) {
            return false;
        }
        ProductTypes other = (ProductTypes) object;
        if ((this.ptname == null && other.ptname != null) || (this.ptname != null && !this.ptname.equals(other.ptname))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "umbrella_suite.model.ProductTypes[ ptname=" + ptname + " ]";
    }
    
}

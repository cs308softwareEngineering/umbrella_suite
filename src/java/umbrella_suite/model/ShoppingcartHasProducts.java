/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mbenlioglu
 */
@Entity
@Table(name = "shoppingcart_has_products")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ShoppingcartHasProducts.findAll", query = "SELECT s FROM ShoppingcartHasProducts s"),
    @NamedQuery(name = "ShoppingcartHasProducts.findByScid", query = "SELECT s FROM ShoppingcartHasProducts s WHERE s.shoppingcartHasProductsPK.scid = :scid"),
    @NamedQuery(name = "ShoppingcartHasProducts.findByPid", query = "SELECT s FROM ShoppingcartHasProducts s WHERE s.shoppingcartHasProductsPK.pid = :pid"),
    @NamedQuery(name = "ShoppingcartHasProducts.findByNumberOfProduct", query = "SELECT s FROM ShoppingcartHasProducts s WHERE s.numberOfProduct = :numberOfProduct")})
public class ShoppingcartHasProducts implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ShoppingcartHasProductsPK shoppingcartHasProductsPK;
    @Column(name = "number_of_product")
    private Integer numberOfProduct;

    public ShoppingcartHasProducts() {
    }

    public ShoppingcartHasProducts(ShoppingcartHasProductsPK shoppingcartHasProductsPK) {
        this.shoppingcartHasProductsPK = shoppingcartHasProductsPK;
    }

    public ShoppingcartHasProducts(int scid, int pid) {
        this.shoppingcartHasProductsPK = new ShoppingcartHasProductsPK(scid, pid);
    }

    public ShoppingcartHasProductsPK getShoppingcartHasProductsPK() {
        return shoppingcartHasProductsPK;
    }

    public void setShoppingcartHasProductsPK(ShoppingcartHasProductsPK shoppingcartHasProductsPK) {
        this.shoppingcartHasProductsPK = shoppingcartHasProductsPK;
    }

    public Integer getNumberOfProduct() {
        return numberOfProduct;
    }

    public void setNumberOfProduct(Integer numberOfProduct) {
        this.numberOfProduct = numberOfProduct;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shoppingcartHasProductsPK != null ? shoppingcartHasProductsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShoppingcartHasProducts)) {
            return false;
        }
        ShoppingcartHasProducts other = (ShoppingcartHasProducts) object;
        if ((this.shoppingcartHasProductsPK == null && other.shoppingcartHasProductsPK != null) || (this.shoppingcartHasProductsPK != null && !this.shoppingcartHasProductsPK.equals(other.shoppingcartHasProductsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "umbrella_suite.model.ShoppingcartHasProducts[ shoppingcartHasProductsPK=" + shoppingcartHasProductsPK + " ]";
    }
    
}

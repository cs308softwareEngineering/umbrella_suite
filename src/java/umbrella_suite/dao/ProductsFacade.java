/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.Products;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class ProductsFacade extends AbstractFacade<Products> implements ProductsFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductsFacade() {
        super(Products.class);
    }
    
    @Override
    public List<Products> findByType(String department) {
        List<Products> results = em.createQuery(
                "SELECT p FROM Products p WHERE p.ptname = :ptname").setParameter("ptname", department).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No products with given department: "+department);
            return results;
        }
        else
        {
            System.err.println("Products with given department: "+department);
            return results;
        }
    }

    //this function controls product count after checkout... If not available, give an error... If available also increarethe products...
    @Override
    public String controlProductQuantity(ArrayList<Integer> numbers, ArrayList<Integer> pids) {        
        List<Products> products = new ArrayList<>();
        for(int i=0; i<pids.size(); i++){
            Products p = this.find(pids.get(i));
            products.add(p);
        }
        
        //check availability of products...
        boolean result = true;
        for(int i=0; i<products.size(); i++){
            if(products.get(i).getCount() < numbers.get(i)){
                result = false;
                System.err.println("Checkout failed");
                String error = products.get(i).getPname() + " has just been out of Stock:(";
                return error;
            }
        }
        
        //available product count, then update the database...
        if(result == true){
            System.err.println("Checkout completed! Product count is set again!");
            for(int i=0; i<products.size(); i++){
                Products p_new = products.get(i);
                p_new.setCount(products.get(i).getCount() - numbers.get(i));
                this.edit(p_new);
            }
            return "YES";
        }else{
            return "NO";
        }
    }

    //set new Prices for product by given pid
    @Override
    public void setNewPrices(Integer pid, Double price, Double discount) {
        Products newProduct = this.find(pid);
        if(newProduct != null){
            newProduct.setPrice(price);
            newProduct.setDiscount(discount);
            this.edit(newProduct);  
        }else if(newProduct == null){
            System.err.println("null exception! with pid" + pid + " in function ProductsFacade.setNewPrices()");
        }
    }

    //set new quantity to product by given pid
    @Override
    public void setQuantity(Integer pid, Integer quantity) {
        Products newProduct = this.find(pid);
        if(newProduct != null){
            newProduct.setCount(quantity);
            this.edit(newProduct);  
        }else if(newProduct == null){
            System.err.println("null exception! with pid" + pid + " in function ProductsFacade.setQuantity()");
        }
    }

    @Override
    public Products processProduct(String productName3, Integer quantity3, String department3, Double newPrice3, String description3, String image3) {
        Products temp = new Products();
        temp.setPname(productName3);
        temp.setCount(quantity3);
        temp.setPtname(department3);
        temp.setDescription(description3);
        temp.setPrice(newPrice3);
        temp.setDiscount(0.0);
        temp.setImage(image3);
        System.err.println("New Products are entered safely... It will return...");
        return temp;
    }
}

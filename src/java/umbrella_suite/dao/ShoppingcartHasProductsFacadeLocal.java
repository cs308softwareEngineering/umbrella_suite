/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.ShoppingcartHasProducts;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface ShoppingcartHasProductsFacadeLocal {

    void create(ShoppingcartHasProducts shoppingcartHasProducts);

    void edit(ShoppingcartHasProducts shoppingcartHasProducts);

    void remove(ShoppingcartHasProducts shoppingcartHasProducts);

    ShoppingcartHasProducts find(Object id);
    
    ShoppingcartHasProducts find(int scid, int pid); //test it
    
    ArrayList<ShoppingcartHasProducts> find(int scid); //test it

    List<ShoppingcartHasProducts> findAll();

    List<ShoppingcartHasProducts> findRange(int[] range);

    int count();
    
    ShoppingcartHasProducts processProductShopping(int scid, int pid); //test it
    
    void incrementProductQuantity(ShoppingcartHasProducts hasProducts); //test it
    
    int countByScid(int scid); //test it
    
}

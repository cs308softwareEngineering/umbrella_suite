/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.Orders;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class OrdersFacade extends AbstractFacade<Orders> implements OrdersFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdersFacade() {
        super(Orders.class);
    }
    
    @Override
    public Orders findByScid(int scid) {
        List<Orders> results = em.createQuery(
                "SELECT o FROM Orders o WHERE o.scid = :scid").setParameter("scid", scid).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No orders with scid: "+scid);
            return null;
        }
        else
        {
            System.err.println("Order with scid: "+scid);
            return results.get(0);
        }
    }

    @Override
    public void setDelivered(int oid) {
          Orders order = this.find(oid);
          if(order != null){
                order.setOrderstatus("delivered");
                this.edit(order); 
          }       
    }

    @Override
    public Orders processOrder(int scid) {
        Orders temp = new Orders();
        temp.setScid(scid);
        temp.setOrderstatus("hold");
        return temp;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.ProductTypes;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface ProductTypesFacadeLocal {

    void create(ProductTypes productTypes);

    void edit(ProductTypes productTypes);

    void remove(ProductTypes productTypes);

    ProductTypes find(Object id);

    List<ProductTypes> findAll();

    List<ProductTypes> findRange(int[] range);

    int count();
    
}

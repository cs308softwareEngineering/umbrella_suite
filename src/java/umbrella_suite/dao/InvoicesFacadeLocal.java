/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.Invoices;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface InvoicesFacadeLocal {

    void create(Invoices invoices);

    void edit(Invoices invoices);

    void remove(Invoices invoices);

    Invoices find(Object id);
    
    Invoices findByOrder (int oid);

    List<Invoices> findAll();

    List<Invoices> findRange(int[] range);

    int count();
    
    Invoices processInvoice(String address, Integer oid, String description); //test it
    
}

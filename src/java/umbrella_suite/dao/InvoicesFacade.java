/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.Invoices;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class InvoicesFacade extends AbstractFacade<Invoices> implements InvoicesFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InvoicesFacade() {
        super(Invoices.class);
    }

    @Override
    public Invoices processInvoice(String address, Integer oid, String description) {
        System.err.println("umbrella_suite.dao.InvoicesFacade.processInvoice()");
        Invoices temp = new Invoices();
        temp.setAddress(address);
        temp.setDescription(description);
        temp.setOid(oid);
        return temp;
    }

    @Override
    public Invoices findByOrder(int oid) {
        List<Invoices> results = em.createQuery(
                "SELECT i FROM Invoices i WHERE i.oid = :oid").setParameter("oid", oid).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No invoices with oid: "+oid);
            return new Invoices();
        }
        else
        {
            System.err.println("invoices with oid: "+oid);
            return results.get(0);
        }
		
    }
    
    
    
}

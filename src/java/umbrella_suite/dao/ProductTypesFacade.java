/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.ProductTypes;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class ProductTypesFacade extends AbstractFacade<ProductTypes> implements ProductTypesFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductTypesFacade() {
        super(ProductTypes.class);
    }
    
}

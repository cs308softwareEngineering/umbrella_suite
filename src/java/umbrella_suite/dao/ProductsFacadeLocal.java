/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.Products;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface ProductsFacadeLocal {

    void create(Products products);

    void edit(Products products);

    void remove(Products products);

    Products find(Object id);
    
    List<Products> findByType(String department);

    List<Products> findAll();

    List<Products> findRange(int[] range);

    int count();
    
    String controlProductQuantity(ArrayList<Integer> numbers, ArrayList<Integer> pids); //test it   
    
    void setNewPrices(Integer pid, Double price, Double discount);
    
    void setQuantity(Integer pid, Integer quantity);
    
    Products processProduct(String productName3, Integer quantity3, String department3, Double newPrice3, String description3, String image3);
}

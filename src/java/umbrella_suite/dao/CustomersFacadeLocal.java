/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.Customers;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface CustomersFacadeLocal {

    void create(Customers customers);

    void edit(Customers customers);

    void remove(Customers customers);

    Customers find(Object id);
    
    Customers find(String email);

    List<Customers> findAll();

    List<Customers> findRange(int[] range);

    int count();
    
    //void addUser(Customers customer);
    
    void setNewAddress(String email, String address); //test it
    
    void setNewPassword(String email, String password); //test it
    
}

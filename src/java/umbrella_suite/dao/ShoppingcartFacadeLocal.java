/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.Shoppingcart;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface ShoppingcartFacadeLocal {

    void create(Shoppingcart shoppingcart);

    void edit(Shoppingcart shoppingcart);

    void remove(Shoppingcart shoppingcart);

    Shoppingcart find(Object id);
    
    int find(String email); //return shoppingcart scid with corresponding email //test it
    
    List<Shoppingcart> findOrdered(String email); //return all shoppingCart with email

    List<Shoppingcart> findAll();

    List<Shoppingcart> findRange(int[] range);

    int count();
    
    Shoppingcart processShoppingCart(String email); //test it
    
}

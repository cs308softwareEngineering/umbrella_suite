/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.Customers;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class CustomersFacade extends AbstractFacade<Customers> implements CustomersFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomersFacade() {
        super(Customers.class);
    }

    @Override
    public Customers find(String email) {
        List<Object> results = em.createQuery(
                "SELECT c FROM Customers c WHERE c.email= :email").setParameter("email", email).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No customer with email: "+email);
            return null;
        }
        else
        {
            System.err.println("customer with email: "+email);
            return (Customers)results.get(0);
        }
		
    }

//    @Override
//    public void addUser(Customers customer) {
//        em.persist(customer);
//    }
    
    @Override
    public void setNewAddress(String email, String address) {
        Customers customer = this.find(email);
        if(customer != null){
             customer.setAddress(address);
             this.edit(customer);
        } 
    }

    @Override
    public void setNewPassword(String email, String password) {
        Customers customer = this.find(email);
        if(customer != null){
            customer.setPwd(password);
            this.edit(customer);
        }
    }
    
}

<%-- 
    Document   : TicketBrowsinPage
    Created on : May 8, 2016, 5:34:48 PM
    Author     : ozergence
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="java.util.ArrayList" %>
<%@page import="umbrella_suite.model.Tickets" %>

<!DOCTYPE html>

<%@include file="Login.jsp"%>

<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<div class="col-sm-1"></div>
<div class="col-sm-10">
<table class="table table-responsive">
    <thead>
      <tr>
        <th>Order id</th>
        <th>Problem</th>
        <th>Status</th>
        <th>Reply</th>
      </tr>
    </thead>     
    <tbody>
        <% ArrayList<Tickets> tickets = (ArrayList<Tickets>)request.getAttribute("tickets");
            String status,reply,problem;           
            Integer orderId;
            
            for (int i = 0; i < tickets.size();i++){
                 orderId = tickets.get(i).getOid();
                 if(tickets.get(i).getActive()){
                     status = "Active";
                 }else{
                     status = "Resolved";
                 }
                 problem = tickets.get(i).getProblem();
                 reply = tickets.get(i).getReply();
        %>  
        <tr>
            <td>
                <%=orderId%>
            </td>
            <td>
                <textarea class="form-control" rows="3" disabled><%=problem%></textarea>
            </td>
            <td>                 
                <%=status%>
            </td>
            <td>
                <textarea class="form-control" rows="3" disabled><%=reply%></textarea>
            </td>
                
        </tr>
        <%}%>
</tbody>
</div>

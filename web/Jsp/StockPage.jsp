<%-- 
    Document   : StockPage
    Created on : May 4, 2016, 1:51:54 AM
    Author     : ozergence
--%>
<%@page import="java.util.List" %>
<%@page import="umbrella_suite.model.Products" %>

<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<%@include file="Login.jsp"%>
<div class="col-sm-2">
    <br><br>
    <form class="form-vertical" action="/umbrella_suite/ManagerController" role="form">
        <label class="control-label" > Actions</label>
        <br>
        <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#addProductModal">Add New Product</button>
        <button class="btn btn-block btn-warning" value="lookInvoices" name="action">View Invoices</button>
    </form>
</div>
<div class="col-sm-10">
<table class="table table-striped">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Set Stocks</th>
      </tr>
    </thead>     
    <tbody>
        <% List<Products> products = (List<Products>)request.getAttribute("products");
            
            String productName,classRow;
            Integer productId,quantity;
            for (Products p: products){
                 productName = p.getPname();
                 productId = p.getPid();
                 quantity = p.getCount();
                 if(quantity<10){
                     classRow = "success";
                 }else{
                     classRow = "warning";
                 }
        %>  
        <tr class = <%=classRow%>>
            <input type="hidden" value="<%=productId%>"/>
            <td>
                <%=productName%>
            </td>
            <td>
                <%=quantity%>
            </td>
            <td>
                <button type="submit" class="btn edit-modal btn-link" data-toggle="modal" data-id="<%=productId%>" data-quantity="<%=quantity%>" data-name="<%=productName%>" href="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button> 
            </td>
                
        </tr>
        <%}%>
</tbody>
</table>
</div>

     
<!-- Stock Set Modal -->
<div id="editModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <label class="control-label col-sm-3" name="productNameField" id="productNameField" for="productNameField"></label>
            </div>
            <div class="modal-body">             
                  <form class="form-horizontal" id="setQuantity" method="post" action="/umbrella_suite/ManagerController"role="form">
                    <input type="hidden" name="productIdField" id="productIdField" value=""/>
                   <div class="form-group">
                      <label class="control-label col-sm-6" for="quantity">New Quantity</label>
                       <input type="text" name="productQuantityField" id="productQuantityField" value=""/>
                   </div>
                    <button class="btn btn-primary btn-block" type="submit" value="setQuantity" name="action" id="setQuantity">Change Quantity</button> 
                  </form>
            </div>
        </div>
    </div>
</div>
        
 <script>                   
     $(document).on("click", ".edit-modal", function () {
         
     var id = $(this).data('id');
     $(".modal-body #productIdField").val(id);
     
      var quantity = $(this).data('quantity');
     $(".modal-body #productQuantityField").val(quantity);

      var name = $(this).data('name');
     document.getElementById("productNameField").innerHTML = name;

});
</script>

<!-- Product Add Modal -->
<div id="addProductModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a new product</h4>
            </div>
            <div class="modal-body">
                
              <form class="form-horizontal" id="addProduct" method="post" action="/umbrella_suite/ManagerController" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Product Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="productName" name="productName" placeholder="Enter product name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="quantity">Quantity:</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Enter quantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="productType">Type:</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="productType" name="productType" placeholder="Enter Product Type">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="price">Price</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="price" name="price" placeholder="Enter price">
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-sm-3" for="description">Description</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="description" name="description" placeholder="Enter Product Description">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="address">Image URL:</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="imageUrl" name="imageUrl" placeholder="Enter image Url">
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary btn-block" value="addProduct" name="action" >Add Product </button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>    

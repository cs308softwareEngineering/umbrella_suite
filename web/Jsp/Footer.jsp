<%-- 
    Document   : Footer
    Created on : Apr 29, 2016, 9:42:26 PM
    Author     : Gence Özer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- footer --> 
<footer class="container-fluid text-center">
<a href="http://www.freepik.com/">Freepik</a> from <a href="http://www.flaticon.com/">Flaticon</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a>. Made with <a href="http://logomakr.com" title="Logo Maker">Logo Maker</a>
  <p>Umbrella Company All Rights Reserved</p>  
</footer>

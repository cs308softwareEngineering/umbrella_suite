<%-- 
    Document   : ProductManagerHeader.jsp
    Created on : May 4, 2016, 1:46:00 AM
    Author     : ozergence
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Creating url for navbars -->
<c:url var="electronicsUrl" value="/MainController">
   <c:param name="action" value="browseProducts"/>
   <c:param name="type" value="Electronics"/>
</c:url>

<c:url var="dressingUrl" value="/MainController">
   <c:param name="action" value="browseProducts"/>
   <c:param name="type" value="Dressing"/>
</c:url>


<c:url var="musicUrl" value="/MainController">
   <c:param name="action" value="browseProducts"/>
   <c:param name="type" value="Music"/>
</c:url>

<c:url var="displayCartUrl" value="/MainController">
   <c:param name="action" value="displayCart"/>
</c:url>

<c:url var="stockManagementPageUrl" value="/ManagerController">
     <c:param name="action" value="stockManagementPage"/>
</c:url>




<l:LoginStatus email="${email}"/>
<l:LoginStatus firstName="${firstName}"/>
<l:LoginStatus lastName="${lastName}"/>
<l:LoginStatus address="${address}"/>
<l:LoginStatus cartCount="${cartCount}"/>

<!--  Customer navigation bar -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="/umbrella_suite/Jsp"><img src="/umbrella_suite/Resources/Logo-thumbnail.png" class="img-rounded" alt="Site Logo" width="32" height="32"></a></li>
                <li><a href="${electronicsUrl}">Electronics</a></li>
                <li><a href="${dressingUrl}">Dressing</a></li>
                <li><a href="${musicUrl}">Music</a></li>
                <li><a href="${stockManagementPageUrl}">Stock Page</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a data-toggle="modal" href="#profileModal"><span class="glyphicon glyphicon-user">${email}</span> </a></li>
                <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart   <span class="badge">${cartCount}</span></a></li>
                <li><form class="form-horizontal" id="signOut" method="post" action="/umbrella_suite/UserController" role="form">
                       <button type="submit" class="btn btn-warning btn-sm" style="vertical-align: middle" value="signOut" name="action">Sign Out</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- ProfileManagement Modal -->
<div id="profileModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Your Profile</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="changeCredentials" method="post" action="/umbrella_suite/UserController" role="form">   
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Email:</label>
                        <div class="col-sm-7">
                            <input class="form-control" id="emailInput" type="text" placeholder="${email}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="firstName">First Name:</label>
                        <div class="col-sm-7">
                            <input class="form-control" id="firstNameInput" type="text" placeholder="${firstName}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="lastName">Last Name:</label>
                        <div class="col-sm-7">
                            <input class="form-control" id="lastNameInput" type="text" placeholder="${lastName}" disabled>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
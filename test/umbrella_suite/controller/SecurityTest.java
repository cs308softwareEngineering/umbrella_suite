/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.controller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author beyzabozbey
 */
public class SecurityTest {
    
    public SecurityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of generatePassword method, of class Security.
     */
    @Test
    public void testGeneratePassword() {
        System.out.println("generatePassword");
        
        String result = Security.generatePassword();
        if(result == null || result.length() != 8){
            fail("Security.testGeneratePassword() fails.");
        }
        else
            System.out.println("umbrella_suite.controller.SecurityTest.testGeneratePassword() passed.");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import umbrella_suite.model.Tickets;

/**
 *
 * @author mbenlioglu
 */
public class TicketsFacadeTest {
    
    public TicketsFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of processTicket method, of class TicketsFacade.
     */
    @Test
    public void testProcessTicket() throws Exception {
        System.out.println("umbrella_suite.dao.TicketsFacadeTest.testProcessTicket() test started!");
        String problem = "test problem";
        Boolean active = Boolean.TRUE;
        String reply = "";
        int oid = 20;
        TicketsFacadeLocal instance = new TicketsFacade();
        Tickets expResult = new Tickets();
        expResult.setOid(oid);
        expResult.setProblem(problem);
        expResult.setActive(active);
        expResult.setReply("");
        Tickets result = instance.processTicket(oid, problem);
        try {
            assertEquals(expResult.getActive(), result.getActive());
            System.out.println("umbrella_suite.dao.TicketsFacadeTest.testProcessInvoice(): Activeness OK.");
            assertEquals(expResult.getProblem(), result.getProblem());
            System.out.println("umbrella_suite.dao.TicketsFacadeTest.testProcessInvoice(): Problem OK");
            assertEquals(expResult.getOid(), result.getOid());
            System.out.println("umbrella_suite.dao.TicketsFacadeTest.testProcessInvoice(): Oid OK");
            assertEquals(expResult.getReply(), result.getReply());
            System.out.println("umbrella_suite.dao.TicketsFacadeTest.testProcessInvoice(): Reply OK");
        } catch (Exception e) {
            System.out.println("umbrella_suite.dao.TicketsFacadeTest.testProcessInvoice(): Wrong process output");
            fail(e.getMessage());
        }
        System.out.println("umbrella_suite.dao.TicketsFacadeTest.testProcessTicket() test succeed!");
    }
    
}

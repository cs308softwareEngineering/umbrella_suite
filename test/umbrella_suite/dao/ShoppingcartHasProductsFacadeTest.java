/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import umbrella_suite.model.ShoppingcartHasProducts;
import umbrella_suite.model.ShoppingcartHasProductsPK;

/**
 *
 * @author mbenlioglu
 */
public class ShoppingcartHasProductsFacadeTest {
    
    public ShoppingcartHasProductsFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of processProductShopping method, of class ShoppingcartHasProductsFacade.
     */
    @Test
    public void testProcessProductShopping() throws Exception {
        System.out.println("umbrella_suite.dao.ShoppingcartHasProductsFacadeTest.testProcessProductShopping(): Test Started...");
        int scid = (int)Math.random();
        int pid = (int)Math.random();
        ShoppingcartHasProductsFacadeLocal instance = new ShoppingcartHasProductsFacade();
        ShoppingcartHasProducts result = instance.processProductShopping(scid, pid);
        try {
            assertEquals(scid, result.getShoppingcartHasProductsPK().getScid());
            System.out.println("umbrella_suite.dao.ShoppingcartHasProductsFacadeTest.testProcessProductShopping(): ShoppingCart ID OK...");
            assertEquals(pid, result.getShoppingcartHasProductsPK().getPid());
            System.out.println("umbrella_suite.dao.ShoppingcartHasProductsFacadeTest.testProcessProductShopping(): Product ID OK...");
        } catch (Exception e) {
            System.err.println("umbrella_suite.dao.ShoppingcartHasProductsFacadeTest.testProcessProductShopping(): Vaues mismatch, Test failed...");
            fail(e.getMessage());
        }
        System.out.println("umbrella_suite.dao.ShoppingcartHasProductsFacadeTest.testProcessProductShopping(): Test Succeed...");
    }
    
}

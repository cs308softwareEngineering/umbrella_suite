/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author mbenlioglu
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({umbrella_suite.dao.ProductsFacadeTest.class, umbrella_suite.dao.OrdersFacadeTest.class, umbrella_suite.dao.ShoppingcartHasProductsFacadeTest.class, umbrella_suite.dao.InvoicesFacadeTest.class, umbrella_suite.dao.ShoppingcartFacadeTest.class, umbrella_suite.dao.CryptoFacadeTest.class, umbrella_suite.dao.TicketsFacadeTest.class,})
public class DaoSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import umbrella_suite.model.Shoppingcart;

/**
 *
 * @author mbenlioglu
 */
public class ShoppingcartFacadeTest {

    public ShoppingcartFacadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of processShoppingCart method, of class ShoppingcartFacade.
     */
    @Test
    public void testProcessShoppingCart() throws Exception {
        System.out.println("umbrella_suite.dao.ShoppingcartFacadeTest.testProcessShoppingCart(): Test Started...");
        String email = "test@email.com";
        ShoppingcartFacadeLocal instance = new ShoppingcartFacade();
        Shoppingcart expResult = new Shoppingcart();
        expResult.setEmail(email);
        Shoppingcart result = instance.processShoppingCart(email);
        try {
            assertEquals(expResult.getEmail(), result.getEmail());
            System.out.println("umbrella_suite.dao.ShoppingcartFacadeTest.testProcessShoppingCart(): E-mail OK...");
        } catch (Exception e) {
            System.err.println("umbrella_suite.dao.ShoppingcartFacadeTest.testProcessShoppingCart(): Outputs mismatch, Test Failed...");
            fail(e.getMessage());
        }
        System.out.println("umbrella_suite.dao.ShoppingcartFacadeTest.testProcessShoppingCart(): Test Succeed...");
    }

}

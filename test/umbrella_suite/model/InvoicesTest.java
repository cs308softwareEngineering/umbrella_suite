/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.model;

import java.lang.reflect.Field;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author beyzabozbey
 */
public class InvoicesTest {
    
    public InvoicesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIid method, of class Invoices.
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.NoSuchFieldException
     */
    @Test
    public void testGetIid() throws IllegalAccessException, NoSuchFieldException {
        System.out.println("getIid");
        final Invoices instance = new Invoices();
        final Field field = instance.getClass().getDeclaredField("iid");
        field.setAccessible(true);
        field.set(instance, 5);
        final int result = instance.getIid();
        try
        {
            assertEquals(5, result);
        }catch(AssertionError e)
        {
            fail("Invoices.getIid() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testGetIid() passed.");
    }

    /**
     * Test of setIid method, of class Invoices.
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.NoSuchFieldException
     */
    @Test
    public void testSetIid() throws IllegalAccessException, NoSuchFieldException {
        System.out.println("setIid");
        final Invoices instance = new Invoices();
        instance.setIid(5);
        final Field field = instance.getClass().getDeclaredField("iid");
        field.setAccessible(true);
        try
        {
            assertEquals(5, field.get(instance));
        }catch(AssertionError e)
        {
            fail("Invoices.setIid() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testSetIid() passed.");
    }

    /**
     * Test of getOid method, of class Invoices.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetOid() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getOid");
        final Invoices instance = new Invoices();
        final Field field = instance.getClass().getDeclaredField("oid");
        field.setAccessible(true);
        field.set(instance, 5);
        final int result = instance.getOid();
        try
        {
            assertEquals(5, result);
        }catch(AssertionError e)
        {
            fail("Invoices.getOid() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testGetOid() passed.");
    }

    /**
     * Test of setOid method, of class Invoices.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetOid() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setOid");
        final Invoices instance = new Invoices();
        instance.setOid(5);
        final Field field = instance.getClass().getDeclaredField("oid");
        field.setAccessible(true);
        try
        {
            assertEquals(5, field.get(instance));
        }catch(AssertionError e)
        {
            fail("Invoices.setOid() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testSetOid() passed.");
    }

    /**
     * Test of getDescription method, of class Invoices.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetDescription() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getDescription");
        final Invoices instance = new Invoices();
        final Field field = instance.getClass().getDeclaredField("description");
        field.setAccessible(true);
        field.set(instance, "value");
        final String result = instance.getDescription();
        try
        {
            assertEquals("value", result);
        }catch(AssertionError e)
        {
            fail("Invoices.getDescription() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testGetDescription() passed.");
    }

    /**
     * Test of setDescription method, of class Invoices.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetDescription() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setDescription");
        final Invoices instance = new Invoices();
        instance.setDescription("value");
        final Field field = instance.getClass().getDeclaredField("description");
        field.setAccessible(true);
        try
        {
            assertEquals("value", field.get(instance));
        }catch(AssertionError e)
        {
            fail("Invoices.setDescription() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testSetDescription() passed.");
    }

    /**
     * Test of getAddress method, of class Invoices.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetAddress() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getAddress");
        final Invoices instance = new Invoices();
        final Field field = instance.getClass().getDeclaredField("address");
        field.setAccessible(true);
        field.set(instance, "value");
        final String result = instance.getAddress();
        try
        {
            assertEquals("value", result);
        }catch(AssertionError e)
        {
            fail("Invoices.getAddress() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testGetAddress() passed.");
    }

    /**
     * Test of setAddress method, of class Invoices.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetAddress() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setAddress");
        final Invoices instance = new Invoices();
        instance.setAddress("value");
        final Field field = instance.getClass().getDeclaredField("address");
        field.setAccessible(true);
        try
        {
            assertEquals("value", field.get(instance));
        }catch(AssertionError e)
        {
            fail("Invoices.setAddress() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testSetAddress() passed.");
    }

    /**
     * Test of hashCode method, of class Invoices.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Invoices instance = new Invoices();
        int expResult = 0;
        instance.setIid(null);
        int result = instance.hashCode();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Invoices.hashCode() null description fails.");
        }
        Integer iid = 5;
        instance.setIid(iid);
        expResult = iid.hashCode();
        result = instance.hashCode();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Invoices.hashCode() fails.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testHashCode() passed.");
    }

    /**
     * Test of equals method, of class Invoices.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Invoices instance = new Invoices();
        boolean result;
        instance.setAddress("sabanci");
        instance.setDescription("value");
        instance.setIid(5);
        instance.setOid(10);
        Invoices instance2 = new Invoices();
        instance2.setAddress("sabanci");
        instance2.setDescription("value");
        instance2.setIid(5);
        instance2.setOid(10);
        result = instance.equals(instance2);
        try
        {
            assertEquals(true, result);
        }catch(AssertionError e)
        {
            fail("Invoices.equals() equal failed.");
        }
        instance2.setIid(20);
        result = instance.equals(instance2);
        try
        {
            assertEquals(false, result);
        }catch(AssertionError e)
        {
            fail("Invoices.equals() not equal failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testEquals() passed.");
    }

    /**
     * Test of toString method, of class Invoices.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Invoices instance = new Invoices();
        instance.setIid(5);
        String expResult = "umbrella_suite.model.Invoices[ iid=5 ]";
        String result = instance.toString();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Invoices.toString() failed.");
        }
        System.out.println("umbrella_suite.model.InvoicesTest.testToString() passed.");
    }
    
}
